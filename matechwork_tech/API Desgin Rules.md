# API Desgin Rules
### 2. Framework and package sử dụng
Framework sử dụng trong dự án : Lumen (v5.7)
Package hỗ trợ viết api : Dingo 
> Lumen : `https://lumen.laravel.com`
> Dingo api: `https://github.com/dingo/api `


### 6. API RULES DESIGN
- **Authentication**
  - Authentication sử dụng trong dự án : JWT 
  - Anh chị xem tài liệu ở đây : 
    -   - Trang chủ JWT : `https://jwt-auth.readthedocs.io/en/develop` 
    -   - Github: `https://github.com/tymondesigns/jwt-auth`
- **Các chính method sử dụng **
  - **GET (SELETECT) : **Trả về một Resource hoặc một danh sách các resource
  - **POST (CREATE) :** Tạo mới một resource 
  - **PUT (UPDATE) :** Cập nhật thông tin cho một resource
  - **PATCH (UPDATE) :** Cập nhật một thành phần , thuộc tính của Resource
  - **DELETE (DELETE) :** Xóa một Resoure
- **Tên sử dụng danh từ số nhiều , không dùng động từ **
```php
GET /posts - Trả về danh sách những bài viết
GET /posts/1 - Trả về bài viết được định danh
POST /posts - Tạo mới một bài viết
PUT /posts/1 - Cập nhật thông tin cho bài viết #1
PATCH /posts/1 - Cập nhật thuộc tính cho bài viết #1
DELETE /posts/1 - Xóa bài viết #1
```
- **Nếu tồn tại một mối quan hệ duy nhất với Resource khác **
```php
GET /posts/12/comments - Trả về danh sách comment của post #12
GET /posts/12/comments/5 - Trả về comment #5 của post #12
POST /posts/12/comments - tạo mới một comment trong post #12
PUT /posts/12/comments/5 - Cập nhật comment của post #12
PATCH /posts/12/comments/5 - Cập nhật một số thuộc tính của comment #5 cho post #12
DELETE /posts/12/comments/5 - Xóa comment #5 của post #12 
```
- **Không dùng động từ**
```html
<strike> GET /getAllCars </strike> // sai
<strike> POST /createNewCar </strike> // sai
```
- **Lọc kết quả , sắp xếp , tìm kiếm , phân trang**
  1. **Sắp xếp : **sử dụng tham biến " sort " để mô tả luật sắp xếp
```markdown
* Sắp xếp :
GET /posts?sort=-view - Trả về danh sách bài viết được sắp xếp theo view giảm dần
GET /posts?sort=-view,created_at - Trả về danh sách những bài viết được sắp xếp view giảm dần và với bài viết cùng view , những bài viết cũ sẽ được sắp xếp trước
```
**     2 . Lọc kết quả : sử dụng biến duy nhất cho mỗi trường được lọc**
```markdown
GET /posts?state=hot - Lấy danh sách bài viết hot
GET /posts?views>=100 - Lấy danh sách bài viết có lượt view lớn hơn 100 
```
    
    **  3 . Tìm kiếm **
```markdown
GET /posts?q=keyword&state=open&sort=-view,created_at 
```
**      4 . Giới hạn kết quả trả về : Sử dụng tham biến "fields"**
```markdown
 GET /posts?fields=id,title,description,content&sort=-updated_at - Lấy id, title, description,content của bài viết và sắp xếp giảm dần theo updated_at
```
        **5 . Phân trang**
```markdown
Sử dụng Page và per_page
GET /posts?page=1&per_page=100
Hoặc dùng limit và offset
GET /posts?offset=10&limit=100
```
**        6 . Mã HTTP và ý nghĩa**
```markdown
200 OK - Trả về thành công cho những phương thức GET , PUT, PATCH hoặc DELETE
201 Created - Trả về khi một Resource vừa được tạo thành công
204 No Content - Trả về khi Resource xóa thành công
304 Not Modified - Client có thể sử dụng dữ liệu cache
400 Bad Request - Request không hợp lệ
401 Unauthorized - Request cần có sự authentication
403 Forbidden - Server hiểu request nhưng từ chối không cho phép ( không có quyền )
404 Not Found - Không tìm thấy Resource từ URL
405 Method Not Allow - Phương thức không cho phép với user hiện tại ( Sai phương thức )
410 Gone - Resource không tồn tại, version cũ đã không hỗ trợ
415 Unsupported Media Type
422 Unprocessable Entity – Dữ liệu không được kiểm chứng
429 Too Many Requests – Request bị từ chối do bị giới hạn

```
