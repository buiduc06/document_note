**Hướng dẫn Docker cho người mới bắt đầu**

1.	Cài đặt : docker và docker-compose

``` 
# Cài đặt trên windows: 
https://hub.docker.com/editions/community/docker-ce-desktop-windows

# Cài đặt trên linux
https://docs.docker.com/install/linux/docker-ce/ubuntu/

# Một số hướng dẫn
https://viblo.asia/p/docker-compose-dung-moi-truong-cho-ung-dung-laravel-WrJvYEYJvVO

# cài đặt docker-compose
https://docs.docker.com/compose/install/

```

Docker-compose dựng môi trường cho laravel

Xây dựng môi trường

Hãy mở Terminal và thao tác:

```
    # Tạo thư mục chứa source code
    $ mkdir compose
    $ cd compose
    # Tạo một Laravel project. (Mình làm với Laravel 5.3 nhé)
    $ composer create-project --prefer-dist laravel/laravel 
```

Quá trình tạo Laravel project sẽ mất một khoảng thời gian. Hãy kiên nhẫn chờ đợi nhé. ^^.

Tiếp theo,

```
# Root is /compose$
# Tạo thư mục chứa nginx config
  $ mkdir nginx_conf
# Tạo file docker-compose.yml
  $ touch docker-compose.yml
# Tạo file config cho nginx
  $ cd nginx_conf
  $ touch default.conf
```

Sau khi hoàn thành, mở file docker-compose.yml và default.conf bằng sumblime text chỉnh sửa với nội dung sau:

- `docker-compose.yml`: Trong file này chúng ta sẽ tiến hành khai báo các service cần 

```
	version: '2'
	services:
	  application:
	    image: ubuntu:16.04
	    volumes:
	      - ../matechwork_admin:/var/www/html/blog
	  mariadb:
	    image: mariadb
	    ports:
	      - "3696:3306"
	    environment:
	      MYSQL_ROOT_PASSWORD: root
	      MYSQL_DATABASE: mydb
	      MYSQL_USER: guest
	      MYSQL_PASSWORD: 123456Aa@
	    volumes:
	      - ./mariadb:/var/lib/mysql
	  php:
	    image: arodax/php7.2-fpm
	    ports:
	      - "9696:9000"
	    volumes_from:
	      - application
	    links:
	      - mariadb:mysql
	  nginx:
	    image: nginx
	    ports:
	      - "8696:80"
	    links:
	      - php
	    volumes_from:
	      - application
	    volumes:
	      - ./logs/nginx/:/var/log/nginx
	      - ./nginx_conf:/etc/nginx/conf.d
	
```

Giải thích: 

Ở đây tôi đã định nghĩa ra 4 services cần thiết để chạy cho ứng dụng đó là: `application`, `mariadb`, `php` và `nginx`.

-	`application service:`

-	Sử dụng `euclid1990/ubuntu-server` image được pull về từ trang chủ của docker.hub

-	Mounts tất cả project trong thư mục `/blog` ở trên máy của bạn vào thư mục `/var/www/html/blog` trong containers. 
Việc này cho phép bạn sửa code mà không cần phải rebuilt lại image. (sửa code -> reload trình duyệt).

-	`mariadb service`:

-	Sử dụng mariadb image được pull về từ docker.hub

-	Chuyển từ cổng 3306 trên container đến cổng 3696 trên máy của 

-	environment khai báo các tham số

- `	php service`

-	`volumes_from`: Thực hiện tất cả các câu lệnh volumns từ container application

-	`links`: Liên kết php server với mariadb server

- 	`nginx`: Không còn khái niệm nào mới để giải thích. :v 

Tiếp theo là file   `default.conf`


```
server {
         listen 80;
         listen [::]:80 ipv6only=on;
        #listen 80 default_server;
         # Log files for Debugging
         access_log /var/log/nginx/laravel-access.log;
         error_log /var/log/nginx/laravel-error.log;
 
         # Webroot Directory for Laravel project
         root /var/www/html/blog/public;
         index index.php index.html index.htm;
 
         # Your Domain Name
         server_name localhost;
 
         location / {
                 try_files $uri $uri/ /index.php?$query_string;
         }
 
         # PHP-FPM Configuration Nginx
         location ~ \.php$ {
                 try_files $uri =404;
                 fastcgi_split_path_info ^(.+\.php)(/.+)$;
                 fastcgi_pass unix:/run/php/php7.2-fpm.sock;
                 fastcgi_index index.php;
                 fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                 include fastcgi_params;
         }
 }

```

Giải thích qua đổi chút, trong file này định nghĩa server name cho ứng dụng. 

Khai báo thư mục gốc của file index.php, đường dẫn của file `error.log` và `access.log`. 

Chú ý rằng, file index.php của laravel nằm trong /public/index.php vì vậy khi định nghĩa root hãy chỉ cụ thể vào đến puclic nhé.

Các bước chuẩn bị đã sẵn sàng. 

Thư mục dự án của chúng ta bây giờ sẽ có.

Tiến hành chạy:
``` 
//Root là compose
$ docker-compose up 
```
 
Chúng ta dễ dàng nhận thấy là đầu tiên docker tiến hành start 4 service mà chúng ta đã định nghĩa trong file docker-compose.yml.

 Sau khi `docker-compose` chạy xong. Hãy truy cập vào địa chỉ: `localhost:8696` và tận hưởng thành quả.
 
Hoho, màn hình welcome của laravel hiện ra cho chúng ta biết là đã thành công.
Bây giờ, mở thêm 1 tab Terminal nữa lên và gõ:

``` 
$ docker ps 
```

Lệnh này sẽ giúp bạn liệt kê tất cả các container đang chạy
 
Dễ dàng thấy được là đang có 4 container của chúng ta đang chạy với đầy đủ các thông số về chúng.
Migrate Database

Về cơ bản là bây giờ ứng dụng của chúng ta đã chạy được. 

Nhưng chưa nên vội mưng quá sớm, bởi vì Laravel còn cung cấp cho chúng ta tính năng migrate các bảng dữ liệu. 

Vậy câu hỏi đặt ra là bây giờ chúng ta tiến hành chạy lệnh php artisan migrate ở đâu? Và làm cách nào để xem được database chúng ta đang có?

Chắc tới đây mọi người cũng đã hiểu vì sao tôi cần định nghĩa 4 service ở trên rồi đúng không?

Database sẽ được lưu trữ trong container mariadb. Thao 

```
$ docker exec -i -t compose_mariadb_1 /bin/bash	# đối với linux
docker exec -i -t compose_mariadb_1 bash		# đối với windows
```

Câu lệnh này sẽ run container compose_mariadb_1. Sau khi container đã chạy, login vào mysql bằng câu lệnh.

   ``` 
   mysql -uguest -p123456Aa@ mydb 
   ```
 
Với username, password, và database được định nghĩa trong file 
`docker_composer.yml`
Để chạy migrate thì cần chạy nó trong container php.

```  
docker exec -i -t compose_php_1 /bin/bash	
docker exec -i -t compose_php_1 bash # đối với windows
``` 

Sau khi run container, gõ:

```
$ cd /html/blog
$ php artisan migrate
```

Kết quả sẽ được như hình bên. Để kiểm tra lại. Hãy quay lại container mariadb. 

Cần lưu ý một chút. Trong file `.env` cần confix các tham số này như sau:
 
Chú ý rằng `HOST` ở đây phải là địa chỉ của `container` chứa database của bạn nhé. Để kiểm tra địa chỉ của container đó (ở đây là `compose_mariadb_1`) hãy chạy

```
$ docker exec -i -t compose_mariadb_1 cat /etc/hosts
```

Và cuối cùng em ấy cũng show ra:
 
Mọi thứ đã sẵn sàng cho bạn. Hãy bắt đầu vào code dự án của bạn thôi. ^^

